// local reviews data
const reviews = [
  {
    id: 1,
    name: "susan smith",
    job: "web developer",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg",
    text:
      "I'm baby meggings twee health goth +1. Bicycle rights tumeric chartreuse before they sold out chambray pop-up. Shaman humblebrag pickled coloring book salvia hoodie, cold-pressed four dollar toast everyday carry",
  },
  {
    id: 2,
    name: "anna johnson",
    job: "web designer",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
    text:
      "Helvetica artisan kinfolk thundercats lumbersexual blue bottle. Disrupt glossier gastropub deep v vice franzen hell of brooklyn twee enamel pin fashion axe.photo booth jean shorts artisan narwhal.",
  },
  {
    id: 3,
    name: "peter jones",
    job: "intern",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
    text:
      "Sriracha literally flexitarian irony, vape marfa unicorn. Glossier tattooed 8-bit, fixie waistcoat offal activated charcoal slow-carb marfa hell of pabst raclette post-ironic jianbing swag.",
  },
  {
    id: 4,
    name: "bill anderson",
    job: "the boss",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
    text:
      "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ",
  },
];

// get items from html

const img = document.getElementById("usr-img");
const usr_name = document.getElementById("usr-name");
const position = document.getElementById("usr-position");
const review = document.getElementById("usr-review");

const btn_prev = document.getElementById("btn-prev");
const btn_next = document.getElementById("btn-next");

// setting inex of array to start

let curruntItem = 0;

// update after each refresh (or update at start)

window.addEventListener("DOMContentLoaded", () => {
  const item = reviews[curruntItem];

  img.src = item.img;
  usr_name.textContent = item.name;
  position.textContent = item.job;
  review.textContent = item.text;
});

// to move next and prev

function showPerson(person) {
  const item = reviews[person];
  img.src = item.img;
  usr_name.textContent = item.name;
  position.textContent = item.job;
  review.textContent = item.text;
}

// btn access when moving next or prev

btn_prev.addEventListener("click", () => {
  curruntItem--;
  if (curruntItem < 0) {
    curruntItem = reviews.length - 1;
  }
  showPerson(curruntItem);
});

btn_next.addEventListener("click", () => {
  curruntItem++;
  if (curruntItem > reviews.length - 1) {
    curruntItem = 0;
  }

  showPerson(curruntItem);
});
